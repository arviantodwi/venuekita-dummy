/*!
 * Data Warehouse's Gruntfile
 * Authored by Arvianto Dwi, 2015-2016
 */
module.exports = function(grunt) {
  'use strict';

  // Load all packages in development scope with "grunt-" file name pattern.
  // See package.json for packages list and detail.
  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});

  // Set the version of build target configuration. Use '--v-target=YEAR' option
  // when running a task or grunt will throw a fatal error.
  grunt.config('v-target', grunt.option('v-target') || 'undefined');

  if( grunt.config('v-target') === 'undefined' ) {
    grunt.fail.fatal('ERROR: Version of build target must be specified using \'--v-target\' option');
  }
  else {
    if( String( grunt.config('v-target') ) !== '2015' && String( grunt.config('v-target') ) !== '2016' ) {
      grunt.fail.fatal('ERROR: Build target v.' + grunt.config('v-target') + ' is not found');
    }
    else {
      // Load some postcss plugin and assign each of it to variable
      var autoprefixer = require('autoprefixer');
      var cssnano = require('cssnano');
      var contentDir = (grunt.config('v-target') === '2015') ? 'Content/' : 'Content2016/';

      /**
       * Generate the file path
       *
       * @param   {string}  target
       * @param   {string}  fileName
       * @param   {boolean} isDev
       * @param   {boolean} returnPathOnly
       * @returns {string}
       */
      var getPath = function(target, fileName, isDev, returnPathOnly) {
        var isDev = (typeof isDev !== 'undefined' && typeof isDev === 'boolean') ? isDev : true;
        var returnPathOnly = (typeof returnPathOnly !== 'undefined' && typeof returnPathOnly === 'boolean') ? returnPathOnly : false;
        var extension = fileName.split('.').pop();
        var fileType = (extension.toLowerCase() === 'scss' || extension.toLowerCase() === 'sass' || extension.toLowerCase() === 'css') ? 'css' : ( (extension.toLowerCase() === 'js') ? 'js' : 'undefined' );
        var fullPath = target;

        if(fileType === 'undefined') {
          grunt.fail.fatal('ERROR: Cannot determine the file\'s build type. Use only .css, .scss, .sass, or .js extension.');
        }
        else {
          if(fileType === 'css') {
            fullPath += 'css/';

            if(extension === 'scss' || extension === 'sass') {
              fullPath += 'src/';
            }
            else { // .css extension
              if(isDev) {
                fullPath += 'dev/';
              }
            }
          }
          else { // .js extension
            fullPath += 'js/';
          }

          // append file name to path if returnPathOnly is FALSE
          if(!returnPathOnly) {
            fullPath += fileName;
          }
        }
        
        return fullPath;
      };

      // Grunt project configuration
      grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Config: grunt-sass
        sass: {
          compile: {
            options: { outputStyle: 'expanded' },
            files: [
              {
                src: getPath(contentDir, 'venuekita.scss'),
                dest: getPath(contentDir, 'temp.venuekita.css', true)
              },
              {
                src: getPath(contentDir, 'vkCalculator.scss'),
                dest: getPath(contentDir, 'temp.vkCalculator.css', true)
              }
            ]
          }
        },

        // Config: grunt-postcss
        postcss: {
          options: {
            processors: [
              autoprefixer({
                browsers: ['> 1%', 'last 2 versions', 'ie 8', 'ie 9', 'Opera 12.1']
              })
            ]
          },
          development: {
            files: [
              {
                src: getPath(contentDir, 'temp.venuekita.css', true),
                dest: getPath(contentDir, 'venuekita.css', true)
              },
              {
                src: getPath(contentDir, 'temp.vkCalculator.css', true),
                dest: getPath(contentDir, 'vkCalculator.css', true)
              }
            ]
          },
          production: {
            options: {
              processors: [ cssnano() ],
              map: {
                inline: false,
                annotation: getPath(contentDir, '*.css', false, true)
              }
            },
            files: [
              {
                expand: true,
                //cwd: 'Content2016/css/dev',
                src: getPath(contentDir, 'venuekita.css', true),
                dest: getPath(contentDir, 'venuekita.css', false, true),
                ext: '.min.css'
              },
              {
                expand: true,
                src: getPath(contentDir, 'vkCalculator.css', true),
                dest: getPath(contentDir, 'vkCalculator.css', false, true),
                ext: '.min.css'
              }
            ]
          }
        },

        // Config: grunt-exec
        exec: {
          npmUpdate: {
            command: 'echo "Updating npm packages..."; npm update;'
          }
        },

        // Config: grunt-contrib-copy
        copy: {},

        // Config: grunt-contrib-clean
        clean: {
          tempCss: [ getPath(contentDir, 'temp.*.css', true) ]
        },

        // Config: grunt-contrib-watch
        watch: {
          scssDev: {
            files: [
              getPath(contentDir, '*.scss'),
              getPath(contentDir, '**/*.scss')
            ],
            tasks: ['sass:compile', 'postcss:development', 'clean:tempCss']
          }
        }
      });

      // Set grunt task
      grunt.registerTask('default', ['exec:npmUpdate']);
      grunt.registerTask('watch-scss', ['watch:scssDev']);
    }
  }
};