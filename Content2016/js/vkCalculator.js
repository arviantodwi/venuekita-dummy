/*!
 * Created by arviantodwi on 4/12/16.
 * Venuekita Calculator
 */
"use strict";

(function( jq, window, document, undefined ) {
  /**
   *
   * @param {object} element
   * @param {object} options
   * @constructor
   */
  var VKCalculator = function(element, options) {
    /**
     * ======================================================================
     * Properties and global variables
     * ====================================================================== */
    // Default plugin options
    var defaults = {
      debug: false,
      calculatorContainer: undefined,
      fieldsContainer: undefined,
      priceCategory: undefined,
      accommodationPrice: 0,
      weekendDiscount: 0,
      maxPax: 500
    };

    // Default callbacks
    var callbacks = {
      onPluginInit  : undefined,
      onPluginLoaded: undefined,
      onValueChange : undefined
    };

    // Default namespaces
    var namespaces = {
      dayClass: 'vk-day',
      inputClass: 'vk-source'
    };

    // Extend all default options and client-side options into a new properties
    var settings = jq.extend(true, defaults, namespaces, callbacks, options);

    // Private object properties
    var obj = {
      self: this,
      root: jq(element),
      inputControls: [],
      calcData: [],
      totalPrice: 0,
      downPaymentPercentage: 0,
      hasWeekendDiscount: false,
      dayNumberForDiscount: null,
      event: {
        click: 'click.vkc',
        change: 'change.vkc'
      },
      markup: {
        price: {
          wrapper: '<b class="unit-price clearfix" />',
          currency: '<abbr title="Indonesia Rupiah">Rp</abbr>'
        },
        conditionalDescription: '<div class="conditional-description" />'
      }
    };

    /**
     * ======================================================================
     * Methods: Initial functions and constructor
     * ====================================================================== */
    /**
     * @returns {boolean}
     * @private
     */
    var _construct = function() {
      if( _init_dependenciesCheck() === true ) {
        // Start the script below if settings has been set correctly.
        // Start debugging if debug setting is true.
        if( typeof settings.debug === 'boolean' && settings.debug === true ) {
          console.group("Initialize %s", obj.self);
          console.time('Load time');
        }

        // Call the init callback if provided
        if( typeof settings.onPluginInit === 'function' ) { _do_onPluginInit(); }

        // Find all day containers and call the generator
        var dayContainer = obj.root.find('.set-fields-row');
        _init_generateDataSource(dayContainer);
        _init_generateCalculator(obj.calcData);

        // Remove calculator loading markup and show the module content now
        settings.fieldsContainer
          .children('.calculator-init-loading').remove().end()
          .children('.' + settings.dayClass).show(0);
        settings.calculatorContainer
          .children('.calculator-init-loading').remove().end()
          .children('ul').show(0);

        // Stop debugging if debug prototype is true.
        if( typeof settings.debug === 'boolean' && settings.debug === true ) {
          console.log('%s initialized', obj.self);
          console.timeEnd('Load time');
          console.groupEnd();
        }
      }
      else { return false; }
    };

    /**
     * @private
     */
    var _init_dependenciesCheck = function() {
      var dependenciesStatus = false;

      // Check if calculator container object is provided as an option.
      // Break the plugin initialization if this value is undefined.
      if( typeof settings.calculatorContainer === 'undefined' || settings.calculatorContainer === '' ) {
        console.error("Failed to initialize VKCalculator! The 'calculatorContainer' option must be defined with node's ID / Class attribute using native JS DOM getElement or jQuery object.");
      }
      // Check if fields container object is provided as an option. Those fields
      // are important to determine the calculator's data source.
      // Break the plugin initialization if this value is undefined.
      else if( typeof settings.fieldsContainer === 'undefined' || settings.fieldsContainer === '' ) {
        console.error("Failed to initialize VKCalculator! The 'fieldsContainer' option must be defined with node's ID / Class attribute using native JS DOM getElement or jQuery object.");
      }
      // Check if price category is provided as an option.
      // Break the plugin initialization if this value is undefined.
      else if( typeof settings.priceCategory === 'undefined' || settings.priceCategory === '' ) {
        console.error("Failed to determine the calculator's price category! The 'priceCategory' option must be defined with 'perPax', 'perRoom', or 'perMinimumCharge' string value.");
      }
      // Every dependencies was met
      else { dependenciesStatus = true; }

      return dependenciesStatus;
    };

    /**
     * @param {object} dayContainer  Day Container object
     * @private
     */
    var _init_generateDataSource = function(dayContainer) {
      var inputType, inputClass;
      // var formFields, selectedTypeOption, tempPax, tempRoom, tempType, tempInput;
      var formFields, selectedTypeOption, tempPax, tempType, tempInput;
      var accommodationDescText = 'Untuk setiap 1 kamar akomodasi, harga yang dihitung adalah sebesar 2 pax';
      var paxDescText = 'Maksimal jumlah peserta adalah ' + String( settings.maxPax ) + ' orang';

      // Classify all percentage value for calculator
      _set_conditionalPercentageValue();

      // Start DOM manipulation for each day using a loop
      jq.each(dayContainer, function(index, day) {
        // Assign new array element to calcData and inputControls using day
        // as an index.
        obj.calcData[index] = [];
        obj.inputControls[index] = [];

        // Add day class
        day.classList.add(settings.dayClass);

        // Assign all form field object as an array element
        formFields = jq(day).find('.form-field');

        // Start source classes generator loop
        for(var i=0; i<formFields.length; i++) {
          if( i === 0 || i === 3) { // 1st & 4th fields are number field...
            inputType = 'input[type=number]';
            inputClass = (i === 0) ? 'pax' : 'room';
          }
          else if( i === 1 ) { // 2nd fields are select field...
            inputType = 'select';
            inputClass = 'type';
          }
          else if( i === 2 ) { // 3rd fields are checkbox field...
            inputType = 'input[type=checkbox]';
            inputClass = 'accommodation';
          }

          // Assign the temporary input, then
          // add input class and day data attribute to it
          tempInput = formFields.eq(i).find(inputType);
          tempInput[0].classList.add(settings.inputClass, inputClass);
          tempInput[0].dataset.day = index+1;

          // Append the conditional description markup
          if( i === 0 ) {
            jq(obj.markup.conditionalDescription).text(paxDescText).appendTo( formFields.eq(i) );
          }
          else if( i === 3 && settings.priceCategory.toLowerCase() === 'perpax' ) {
            jq(obj.markup.conditionalDescription).text(accommodationDescText).appendTo( formFields.eq(i) );
          }

          // Assign the temporary input as a ready-state control
          obj.inputControls[index][inputClass] = tempInput[0];
        }

        // Start assigning the init calculation data by making the temporary
        // data first
        // console.log(obj.inputControls[index]);
        tempPax = parseInt( obj.inputControls[index]['pax'].value );
        // tempRoom = parseInt( obj.inputControls[index]['room'].value );
        tempType = obj.inputControls[index]['type'].options;
        selectedTypeOption = tempType.selectedIndex;

        // Manipulate the optional fields and attributes
        // 1. Remove Residential related fields if price category is not 'perPax'
        if( settings.priceCategory.toLowerCase() !== 'perpax' || ( settings.priceCategory.toLowerCase() === 'perpax' && settings.accommodationPrice === 0 ) ) {
          // Remove Accommodation and Room fields
          jq([
            obj.inputControls[index]['accommodation'],
            obj.inputControls[index]['room']
          ]).parents('.form-field').remove();

          // Automatically set selected meeting type to Full Day
          tempType.selectedIndex = 0;

          // Remove Residential option
          if( tempType.length > 1 ) {
            for(i=tempType.length-1; i>=0; i--) {
              if( tempType[i].dataset.priceTag.toLowerCase() === 'residential' ) {
                tempType[i].remove();
                break;
              }
            }

            // Also remove Half Day option if price category is 'perMinimumCharge'
            if( settings.priceCategory.toLowerCase() === 'perminimumcharge' ) {
              for(i=tempType.length-1; i>=0; i--) {
                if( tempType[i].dataset.priceTag.toLowerCase() === 'half day' ) {
                  tempType[i].remove();
                  tempType[0].textContent = 'Minimum Charge';
                  tempType[0].dataset.priceTag = 'Min. Charge';
                  break;
                }
              }
            }
          }

          // Remove Residential description
          obj.inputControls[index]['type'].nextElementSibling.remove();
          selectedTypeOption = tempType.selectedIndex;
        }
        else {}

        // 2. Change min and max attribute of Pax, and append its conditional description
        obj.inputControls[index]['pax'].min = 2;
        obj.inputControls[index]['pax'].max = settings.maxPax;

        // 3. Change value, attributes, and conditional description of Accommodation
        if( settings.accommodationPrice > 0 && obj.inputControls[index].hasOwnProperty('room') && settings.priceCategory.toLowerCase() === 'perpax' ) {
          if( parseInt( tempType[selectedTypeOption].dataset.forceAccommodation ) !== 1 ) {
            jq(day)
              .find('.form-field .residential-description').hide(0).end()
              .find('.form-field').eq(3).addClass('disabled-field');

            obj.inputControls[index]['accommodation'].disabled = false;
            obj.inputControls[index]['room'].min = 0;
            obj.inputControls[index]['room'].max = Math.ceil( obj.inputControls[index]['pax'].value / 2 );
            obj.inputControls[index]['room'].disabled = true;
          }
          else if( parseInt( tempType[selectedTypeOption].dataset.forceAccommodation ) === 1 ) {
            jq(day)
              .find('.form-field .residential-description').show(0).end()
              .find('.form-field')
              .eq(2).addClass('disabled-field').end()
              .end()
              .find('.form-field').eq(3).addClass('disabled-field');

            obj.inputControls[index]['accommodation'].disabled = true;
            obj.inputControls[index]['room'].min = 0;
            obj.inputControls[index]['room'].max = 0;
            obj.inputControls[index]['room'].disabled = true;
          }
        }

        // 4. Remove unavailable options from meeting type select
        if( tempType.length > 1 ) {
          for(i = tempType.length - 1; i >= 0; i--) {
            if( tempType[i].hasAttribute('data-remove-on-init') && parseInt(tempType[i].dataset.removeOnInit) === 1 ) {
              tempType[i].remove();
            }
          }
        }

        // Finalize calculation data structure
        obj.calcData[index]['price_idr'] = parseInt( tempType[selectedTypeOption].dataset.priceIdr );
        obj.calcData[index]['total_pax'] = tempPax;

        if( obj.hasWeekendDiscount && jq.isNumeric(obj.dayNumberForDiscount) && obj.dayNumberForDiscount === index ) {
          if( settings.priceCategory.toLowerCase() === 'perpax' ) {
            obj.calcData[index]['disc_idr'] = (settings.weekendDiscount / 100) * (obj.calcData[index]['price_idr'] * obj.calcData[index]['total_pax']);
          }
          else {
            obj.calcData[index]['disc_idr'] = (settings.weekendDiscount / 100) * obj.calcData[index]['price_idr'];
          }
        }
        else { obj.calcData[index]['disc_idr'] = 0; }

        obj.calcData[index]['meeting_type'] = tempType[selectedTypeOption].dataset.priceTag;
        obj.calcData[index]['meeting_pax'] = ( parseInt( tempType[selectedTypeOption].dataset.forceAccommodation ) !== 1 ) ? tempPax : 0;
        obj.calcData[index]['staying_pax'] = ( parseInt( tempType[selectedTypeOption].dataset.forceAccommodation ) === 1 ) ? tempPax : 0;
      });

      // Debug the init calculation data
      if( typeof settings.debug === 'boolean' && settings.debug === true ) {
        console.group('Object Data Summary');

          console.group('Calculation Data');
          console.table(obj.calcData);
          console.groupEnd();

          console.group('Input Controls DOM');
          for(var i=0; i<obj.inputControls.length; i++) {
            console.dir(obj.inputControls[i]);
          }
          console.groupEnd();

        console.groupEnd();
      }
    };

    /**
     * @private
     */
    var _init_generateCalculator = function() {
      // Declare inner wrapper DOM
      var innerWrapper = '<ul style="display: none;" />';
      var domToAppend = [];
      var i = 0;

      // Start create the prices list markup from init price calculation
      while(i < obj.calcData.length) {
        var classSet = (i !== obj.calcData.length - 1) ? "pricing-list clearfix" : "pricing-list last-day clearfix";

        domToAppend[i] = '<li class="' + classSet + '">';
        domToAppend[i] += '<span class="day-label">Hari ke-' + (i+1) + '</span>';

        // Start create the sub price list markup
        for( var j=1; j<=2; j++ ) {
          var subClassSet = ( j === 1 ) ? 'calculation' : 'calculation hidden';
          var subDataSet = ( j === 1 ) ? 'meeting' : 'accommodation';

          domToAppend[i] += '<div class="' + subClassSet + '" data-order-type="' + subDataSet + '">';
          domToAppend[i] += _init_generatePriceMarkup(subDataSet, i);
          domToAppend[i] += '</div>';
        }

        // Append weekend discount if discount condition is true
        if( obj.hasWeekendDiscount && jq.isNumeric(obj.dayNumberForDiscount) && obj.dayNumberForDiscount === i ) {
          var tempDiscountPriceMarkup = obj.markup.price.currency + _numberFormat( obj.calcData[i]['disc_idr'], 2, ',', '.' );

          domToAppend[i] += '<div class="calculation discount" data-order-type="discount">';
          domToAppend[i] += '<div class="price-formula" style="margin-bottom: 0;">';
          domToAppend[i] += '<b class="unit-price clearfix">Diskon ' + settings.weekendDiscount + '%</b><span class="equals">=</span>';
          domToAppend[i] += '</div>';
          domToAppend[i] += '<div class="price-sum">';
          domToAppend[i] += jq( obj.markup.price.wrapper ).append( tempDiscountPriceMarkup ).prop('outerHTML');
          domToAppend[i] += '</div>';
          domToAppend[i] += '</div>';
        }

        domToAppend[i] += '</li>';

        i++;
      }

      // Start create the overall subtotal price and last calculation result
      // i is now 3 (var i = 3)
      domToAppend[i] = _init_generateFinalPriceMarkup();

      // Prepend all generated markup above
      jq(innerWrapper).append( domToAppend.join('') ).prependTo( settings.calculatorContainer );
    };

    /**
     * @param {string} markupKind
     * @param {number} index
     * @returns {string}
     * @private
     */
    var _init_generatePriceMarkup = function(markupKind, index) {
      var priceTemplate = '';
      var priceTypeLabel = ( markupKind === 'meeting' ) ? obj.calcData[index]['meeting_type'] : 'Residential';
      var unitCountValue = ( markupKind === 'meeting' ) ? obj.calcData[index]['total_pax'] : obj.calcData[index]['total_room'];
      var priceSource = ( markupKind === 'meeting' ) ? obj.calcData[index]['price_idr'] : settings.accommodationPrice;
      var multiplier = ( settings.priceCategory === 'perPax' ) ? unitCountValue : 1;

      var tempPriceUnitMarkup = obj.markup.price.currency +
        _numberFormat( priceSource, 2, ',', '.' ) +
        '<br><span>' + priceTypeLabel + '</span>';
      var tempPriceUnitResultMarkup = obj.markup.price.currency +
        _numberFormat( priceSource * multiplier, 2, ',', '.' );

      // Formula markup (left side pricing)
      priceTemplate += '<div class="price-formula">';
      priceTemplate += jq( obj.markup.price.wrapper ).append( tempPriceUnitMarkup ).prop('outerHTML');
      priceTemplate += '<i class="fa fa-times"></i>';
      priceTemplate += (settings.priceCategory === 'perPax') ? '<b class="unit-count">' + unitCountValue + ' pax</b>' : '<b class="unit-count">1 ruang</b>';
      priceTemplate += '<span class="equals">=</span>';
      priceTemplate += '</div>';

      // Sub total summary markup (right side pricing)
      priceTemplate += '<div class="price-sum">';
      priceTemplate += jq( obj.markup.price.wrapper ).append( tempPriceUnitResultMarkup ).prop('outerHTML');
      priceTemplate += '</div>';

      return priceTemplate;
    };

    /**
     * @returns {string}
     * @private
     */
    var _init_generateFinalPriceMarkup = function() {
      _set_totalPrice();

      var htmlLiPreformat = '';
      var resultHtmlFrags = [];
      var fragsLabel = [];

      /*if( obj.hasWeekendDiscount ) {
        fragsLabel.push('Subtotal', 'Diskon ' + settings.weekendDiscount + '%');
      }*/

      if( obj.downPaymentPercentage < 1 ) {
        fragsLabel.push('Total', 'Down Payment ' + obj.downPaymentPercentage*100 + '%');
      }
      else {
        fragsLabel.push('Total / DP ' + obj.downPaymentPercentage*100 + '%');
      }

      var fragsLabelCount = fragsLabel.length;

      // _calculatePriceNominal(obj.totalPrice, settings.weekendDiscount, obj.downPaymentPercentage );
      htmlLiPreformat += '<li class="pricing-list last-result clearfix">';
      htmlLiPreformat += '<i class="fa fa-plus" />';

      for(var i=0; i<fragsLabelCount; i++) {
        var innerHtmlFrag = '';
        var tempPrice = _calculateFinalPriceNominal(fragsLabelCount, i);

        resultHtmlFrags[i] = (fragsLabelCount > 2 && i === 1) ? '<div class="calculation final" style="border-bottom:solid 2px #2D415A; margin-bottom:9px;" />' : '<div class="calculation final" />';

        // Label
        innerHtmlFrag += '<div class="price-formula">';
        innerHtmlFrag += '<span class="final-calc-label">' + fragsLabel[i] + '</span><span class="equals">=</span>';
        innerHtmlFrag += '</div>';
        // Summary
        innerHtmlFrag += '<div class="price-sum">';
        innerHtmlFrag += jq( obj.markup.price.wrapper ).append( obj.markup.price.currency + _numberFormat(tempPrice, 2, ',', '.') ).prop('outerHTML');

        resultHtmlFrags[i] = jq(resultHtmlFrags[i]).append(innerHtmlFrag).prop('outerHTML');
      }

      htmlLiPreformat += '</li>';

      return jq(htmlLiPreformat).append(resultHtmlFrags.join('')).prop('outerHTML');
    };

    /**
     * ======================================================================
     * Methods: Extended outside event
     * ====================================================================== */
    /**
     * @private
     */
    var _do_onPluginInit = function() {
      settings.onPluginInit.call(this, obj.root);
    };

    /**
     * @private
     */
    var _do_onPluginLoaded = function() {};

    /**
     * @private
     */
    var _do_onValueChange = function() {};

    /**
     * ======================================================================
     * Methods: Getter and setter
     * ====================================================================== */
    /**
     * @param {number} index
     * @private
     */
    var _set_accommodationRoomMaxLimit = function(index) {
      var roomField = obj.root.find( '.'+settings.dayClass ).eq(index).find('.room')[0];

      if(typeof roomField !== 'undefined') {
        roomField.max = Math.ceil( obj.calcData[index]['total_pax'] / 2 );

        if( parseInt(roomField.value) > parseInt(roomField.max) ) {
          roomField.value = roomField.max;
          obj.calcData[index]['staying_pax'] = parseInt(roomField.value) * 2;
          obj.calcData[index]['meeting_pax'] = obj.calcData[index]['total_pax'] - obj.calcData[index]['staying_pax'];

          _set_unitOnCalculator('number', 'accommodation', index);
        }
      }
    };

    /**
     * @param {string} inputType
     * @param {string} calcType
     * @param {number} index
     * @private
     */
    var _set_unitOnCalculator = function(inputType, calcType, index) {
      if( inputType === 'number' && calcType === 'meeting' && settings.priceCategory !== 'perPax' ) { return; }

      var jqPriceLi = settings.calculatorContainer.find('li').eq(index);
      var jqPriceMeeting = jqPriceLi.find('.calculation[data-order-type=meeting]');
      var jqPriceAccommodation = jqPriceLi.find('.calculation[data-order-type=accommodation]');
      var unit1Selector = '.price-formula';
      var unit2Selector = '.price-sum .unit-price';
      var htmlFrags = {
        formulaUnit: '',
        summaryUnit: ''
      };

      if( inputType === 'number' ) {
        if( calcType === 'meeting' ) {
          unit1Selector += ' .unit-count';

          if( obj.calcData[index]['staying_pax'] === 0 ) {
            htmlFrags.formulaUnit = String( obj.calcData[index]['total_pax'] ) + ' pax';
            htmlFrags.summaryUnit = obj.markup.price.currency + _numberFormat( (obj.calcData[index]['total_pax'] * obj.calcData[index]['price_idr']), 2, ',', '.' );
          }
          else if( obj.calcData[index]['staying_pax'] > 0 ) {
            if( obj.calcData[index]['meeting_type'] !== 'Residential' ) {
              htmlFrags.formulaUnit = String( obj.calcData[index]['meeting_pax'] ) + ' pax';
              htmlFrags.summaryUnit = obj.markup.price.currency + _numberFormat( (obj.calcData[index]['meeting_pax'] * obj.calcData[index]['price_idr']), 2, ',', '.' );
            }
            else {
              htmlFrags.formulaUnit = String( obj.calcData[index]['staying_pax'] ) + ' pax';
              htmlFrags.summaryUnit = obj.markup.price.currency + _numberFormat( (obj.calcData[index]['staying_pax'] * obj.calcData[index]['price_idr']), 2, ',', '.' );
            }
          }

          jqPriceMeeting
            .find(unit1Selector).html( htmlFrags.formulaUnit ).end()
            .find(unit2Selector).html( htmlFrags.summaryUnit );
        }
        else if( calcType === 'accommodation' ) {
          unit1Selector += ' .unit-count';
          htmlFrags.formulaUnit = String( obj.calcData[index]['staying_pax'] ) + ' pax';
          htmlFrags.summaryUnit = obj.markup.price.currency + _numberFormat( (obj.calcData[index]['staying_pax'] * settings.accommodationPrice), 2, ',', '.');

          jqPriceAccommodation
            .find(unit1Selector).html( htmlFrags.formulaUnit ).end()
            .find(unit2Selector).html( htmlFrags.summaryUnit );

          htmlFrags.formulaUnit = String( obj.calcData[index]['meeting_pax'] ) + ' pax';
          htmlFrags.summaryUnit = obj.markup.price.currency + _numberFormat( (obj.calcData[index]['meeting_pax'] * obj.calcData[index]['price_idr']), 2, ',', '.');

          jqPriceMeeting
            .find(unit1Selector).html( htmlFrags.formulaUnit ).end()
            .find(unit2Selector).html( htmlFrags.summaryUnit );
        }
      }
      else if( inputType === 'select' ) {
        var multiplier = 0;
        unit1Selector += ' .unit-price';

        if( settings.priceCategory === 'perPax' ) {
          if( obj.calcData[index]['staying_pax'] === 0 && obj.calcData[index]['meeting_type'] !== 'Residential' ) {
            multiplier = obj.calcData[index]['total_pax'];
          }
          else if( obj.calcData[index]['staying_pax'] > 0 && obj.calcData[index]['meeting_type'] !== 'Residential' ) {
            multiplier = obj.calcData[index]['meeting_pax'];
          }
          else if( obj.calcData[index]['staying_pax'] > 0 && obj.calcData[index]['meeting_type'] === 'Residential' ) {
            multiplier = obj.calcData[index]['staying_pax'];
          }
        }
        else {
          multiplier = 1;
        }

        htmlFrags.formulaUnit = obj.markup.price.currency + _numberFormat(obj.calcData[index]['price_idr'], 2, ',', '.') + '<br><span>' + obj.calcData[index]['meeting_type'] + '</span>';
        htmlFrags.summaryUnit = obj.markup.price.currency + _numberFormat( (multiplier * obj.calcData[index]['price_idr']), 2, ',', '.');

        if( obj.calcData[index]['meeting_type'] !== 'Residential' ) {
          jqPriceMeeting
            .find(unit1Selector).html( htmlFrags.formulaUnit ).end()
            .find(unit2Selector).html( htmlFrags.summaryUnit );
        }
        else {
          jqPriceMeeting
            .find(unit1Selector).html( htmlFrags.formulaUnit )
              .nextAll('.unit-count').html( String( obj.calcData[index]['staying_pax'] ) + ' pax' ).end()
            .end()
            .find(unit2Selector).html( htmlFrags.summaryUnit ).end();
        }
      }
      else if( inputType === 'discount' ) {
        htmlFrags.summaryUnit = obj.markup.price.currency + _numberFormat(obj.calcData[index]['disc_idr'], 2, ',', '.' );

        jqPriceLi.find('.calculation[data-order-type=discount]')
          .find(unit2Selector).html( htmlFrags.summaryUnit );
      }
    };

    /**
     * @private
     */
    var _set_finalPriceUnitOnCalculator = function() {
      var unitContainers = settings.calculatorContainer.find('li > .calculation.final');

      for(var i=0; i<unitContainers.length; i++) {
        var tempPrice = _calculateFinalPriceNominal(unitContainers.length, i);

        jq( unitContainers[i] ).find('.unit-price').html( obj.markup.price.currency + _numberFormat(tempPrice, 2, ',', '.') );
      }
    };

    /**
     * @private
     */
    var _set_totalPrice = function() {
      var tempTotal = 0;

      for(var i=0; i<obj.calcData.length; i++) {
        var multiplier = ( settings.priceCategory === 'perPax' ) ? obj.calcData[i]['meeting_pax'] : 1;

        tempTotal += multiplier * obj.calcData[i]['price_idr'];

        if( obj.calcData[i]['staying_pax'] > 0 ) {
          tempTotal += obj.calcData[i]['staying_pax'] * settings.accommodationPrice;
        }
        if( obj.calcData[i]['disc_idr'] > 0 ) {
          tempTotal -= obj.calcData[i]['disc_idr'];
        }
      }

      obj.totalPrice = tempTotal;
    };

    /**
     * @private
     */
    var _set_conditionalPercentageValue = function() {
      var dates = {
        start: moment( _get_urlQueryString('startDateTime'), 'DDMMYYYY' ),
        end: moment( _get_urlQueryString('endDateTime'), 'DDMMYYYY' ),
        today: moment( moment().utcOffset("+07:00").format('DDMMYYYY'), 'DDMMYYYY' )
      };
      var daysToMeeting = parseInt( dates.start.diff(dates.today, 'days') );
      var daysRange = moment.range( [dates.start, dates.end] ).toArray('days');

      // Classify down payment percentage
      if( daysToMeeting >= 0 && daysToMeeting <= 3 ) {
        obj.downPaymentPercentage = 1;
      }
      else if( daysToMeeting > 3 && daysToMeeting <= 7 ) {
        obj.downPaymentPercentage = 0.5;
      }
      else if( daysToMeeting > 7 ) {
        obj.downPaymentPercentage = 0.3;
      }

      // Set weekend discount if the schedule has weekend meeting
      for(var i=0; i<daysRange.length; i++) {
        if( moment(daysRange[i]).format('dddd') === 'Saturday' || moment(daysRange[i]).format('dddd') === 'Sunday' ) {
          obj.hasWeekendDiscount = true;
          obj.dayNumberForDiscount = i;
          break;
        }
      }
    };

    /**
     * @param variableName
     * @returns {*}
     * @private
     */
    var _get_urlQueryString = function(variableName) {
      var query = window.location.search.substring(1);
      var vars = query.split("&");

      for (var i=0; i<vars.length; i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variableName) { return pair[1]; }
      }

      return false;
    };

    /**
     * ======================================================================
     * Methods: Helper and other functions
     * ====================================================================== */
    /**
     * @param {number} number
     * @param {number} decimals
     * @param {string} dec_point
     * @param {string} thousands_sep
     * @returns {string}
     */
    var _numberFormat = function(number, decimals, dec_point, thousands_sep) {
      //   example 1: _numberFormat(1234.56);
      //   returns 1: '1,235'
      //   example 2: _numberFormat(1234.56, 2, ',', ' ');
      //   returns 2: '1 234,56'
      //   example 3: _numberFormat(1234.5678, 2, '.', '');
      //   returns 3: '1234.57'
      //   example 4: _numberFormat(67, 2, ',', '.');
      //   returns 4: '67,00'
      //   example 5: _numberFormat(1000);
      //   returns 5: '1,000'
      //   example 6: _numberFormat(67.311, 2);
      //   returns 6: '67.31'
      //   example 7: _numberFormat(1000.55, 1);
      //   returns 7: '1,000.6'
      //   example 8: _numberFormat(67000, 5, ',', '.');
      //   returns 8: '67.000,00000'
      //   example 9: _numberFormat(0.9, 0);
      //   returns 9: '1'
      //  example 10: _numberFormat('1.20', 2);
      //  returns 10: '1.20'
      //  example 11: _numberFormat('1.20', 4);
      //  returns 11: '1.2000'
      //  example 12: _numberFormat('1.2000', 3);
      //  returns 12: '1.200'
      //  example 13: _numberFormat('1 000,50', 2, '.', ' ');
      //  returns 13: '100 050.00'
      //  example 14: _numberFormat(1e-8, 8, '.', '');
      //  returns 14: '0.00000001'

      number = (number + '')
        .replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k)
              .toFixed(prec);
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '')
          .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
          .join('0');
      }
      return s.join(dec);
    };

    /**
     * @param {string}  visibility
     * @param {number}  index
     * @param {boolean} toggleNumberFieldOnly
     * @returns {boolean}
     * @private
     */
    var _toggleAccommodationRoomField = function(visibility, index, toggleNumberFieldOnly) {
      toggleNumberFieldOnly = (typeof toggleNumberFieldOnly === 'boolean') ? toggleNumberFieldOnly : false;

      var jqCalculatorPriceItem = settings.calculatorContainer.find('li').eq(index);
      var jqPriceMeeting = jqCalculatorPriceItem.find('.calculation[data-order-type=meeting]');
      var jqPriceAccommodation = jqCalculatorPriceItem.find('.calculation[data-order-type=accommodation]');

      if( toggleNumberFieldOnly ) {
        if( visibility === 'show' ) {
          obj.inputControls[index]['room'].disabled = false;
          obj.inputControls[index]['room'].min = 1;
          obj.inputControls[index]['room'].value = 1;

          jqPriceAccommodation
            .find('.price-formula .unit-count').text('2 pax').end()
            .find('.price-sum .unit-price').html( obj.markup.price.currency + _numberFormat(settings.accommodationPrice * 2, 2, ',', '.') );
        }
        else if( visibility === 'hide' ) {
          obj.inputControls[index]['room'].disabled = true;
          obj.inputControls[index]['room'].min = 0;
          obj.inputControls[index]['room'].value = 0;
        }

        jqPriceMeeting
          .find('.price-formula .unit-count').text( String(obj.calcData[index]['meeting_pax']) + ' pax').end()
          .find('.price-sum .unit-price').html( obj.markup.price.currency + _numberFormat(obj.calcData[index]['meeting_pax'] * obj.calcData[index]['price_idr'], 2, ',', '.') );

        if( visibility === 'show' || visibility === 'hide' ) {
          jq( obj.inputControls[index]['room'] ).parent().toggleClass('disabled-field');
          jqPriceAccommodation.toggleClass('hidden');
        }
      }
      else {
        var jqResidentialDescription = jq( obj.inputControls[index]['type'] ).next('.residential-description');

        if( visibility === 'show' ) {
          if(
            jq( obj.inputControls[index]['room'] ).parent().hasClass('disabled-field') &&
            jq( obj.inputControls[index]['accommodation'] ).parents('.form-field').hasClass('disabled-field')
          ) {
            // obj.inputControls[index]['room'].min = 1;
            obj.inputControls[index]['accommodation'].disabled = false;
            obj.inputControls[index]['accommodation'].checked = false;

            jqResidentialDescription.hide();
            jq( obj.inputControls[index]['accommodation'] ).parents('.form-field').removeClass('disabled-field');

            _set_accommodationRoomMaxLimit(index);
          }
        }
        else if( visibility === 'hide' ) {
          obj.inputControls[index]['room'].disabled = true;
          obj.inputControls[index]['room'].min = 0;
          obj.inputControls[index]['room'].value = 0;
          obj.inputControls[index]['accommodation'].disabled = true;
          obj.inputControls[index]['accommodation'].checked = false;

          if( ! jq( obj.inputControls[index]['room'] ).parent().hasClass('disabled-field') ) {
            jq( obj.inputControls[index]['room'] ).parent().addClass('disabled-field');
          }

          jq( obj.inputControls[index]['accommodation'] ).parents('.form-field').addClass('disabled-field');

          if( ! jqPriceAccommodation.hasClass('hidden') ) {
            jqPriceAccommodation.addClass('hidden');
          }

          jqResidentialDescription.show();
        }
      }
    };

    /**
     * @private
     */
    var _toggleResidentialOnLimitReach = function() {};

    /**
     * @param calculationCount
     * @param index
     * @returns {number}
     * @private
     */
    var _calculateFinalPriceNominal = function(calculationCount, index) {
      var tempCalculation = 0;

      // Probability of calculation count:
      // ['Total', 'Down Payment']
      // ['Total / DP']
      /*if( calculationCount === 4 ) {
        switch(index) {
          case 0:
            tempCalculation = obj.totalPrice;
            break;
          case 1:
            tempCalculation = obj.totalPrice * (settings.weekendDiscount / 100);
            break;
          case 2:
            tempCalculation = obj.totalPrice - ( obj.totalPrice * (settings.weekendDiscount / 100) );
            obj.totalPrice = tempCalculation;
            break;
          case 3:
            tempCalculation = obj.totalPrice * obj.downPaymentPercentage;
            break;
        }
      }
      else if( calculationCount === 3 ) {
        switch(index) {
          case 0:
            tempCalculation = obj.totalPrice;
            break;
          case 1:
            tempCalculation = obj.totalPrice * (settings.weekendDiscount / 100);
            break;
          case 2:
            tempCalculation = obj.totalPrice - ( obj.totalPrice * (settings.weekendDiscount / 100) );
            obj.totalPrice = tempCalculation;
            break;
        }
      }*/
      if( calculationCount === 2 ) {
        switch(index) {
          case 0:
            tempCalculation = obj.totalPrice;
            break;
          case 1:
            tempCalculation = obj.totalPrice * obj.downPaymentPercentage;
            break;
        }
      }
      else { // calculationCount === 1
        tempCalculation = obj.totalPrice;
      }

      return tempCalculation;
    };

    /**
     * ======================================================================
     * Events
     * ====================================================================== */
    /**
     *
     */
    obj.root.on( obj.event.change, '.'+settings.inputClass, function(ev) {
      ev.stopImmediatePropagation();

      var jqThis = jq(this);
      var jqNodeName = jqThis[0].nodeName.toLowerCase();
      var jqNodeType = jqThis[0].type.toLowerCase();
      var dayNumberAsIndex = parseInt( jqThis[0].dataset.day ) - 1;

      switch(jqNodeName) {
        case 'input':
          if( jqNodeType === 'number' ) {
            if( jq.isNumeric(jqThis[0].value) ) {
              if( parseInt(jqThis[0].value) > parseInt(jqThis[0].max) ) {
                jqThis[0].value = jqThis[0].max;
              }
              else if( parseInt(jqThis[0].value) < parseInt(jqThis[0].min) ) {
                jqThis[0].value = jqThis[0].min;
              }
            }
            else if( isNaN(jqThis[0].value) || jqThis[0].value === '' ) {
              jqThis[0].value = jqThis[0].min;
            }

            if( jqThis.hasClass('pax') ) {
              obj.calcData[dayNumberAsIndex]['total_pax'] = parseInt(jqThis[0].value);

              if( obj.calcData[dayNumberAsIndex]['meeting_type'] !== 'Residential' ) {
                obj.calcData[dayNumberAsIndex]['meeting_pax'] = obj.calcData[dayNumberAsIndex]['total_pax'] - obj.calcData[dayNumberAsIndex]['staying_pax'];
              }
              else {
                obj.calcData[dayNumberAsIndex]['staying_pax'] = obj.calcData[dayNumberAsIndex]['total_pax'];
              }

              if( settings.priceCategory.toLowerCase() === 'perpax' ) {
                _set_accommodationRoomMaxLimit(dayNumberAsIndex);
              }

              _set_unitOnCalculator(jqNodeType, 'meeting', dayNumberAsIndex);
            }
            else if( jqThis.hasClass('room') ) {
              if( parseInt(jqThis[0].value) * 2 < obj.calcData[dayNumberAsIndex]['total_pax'] ) {
                obj.calcData[dayNumberAsIndex]['staying_pax'] = parseInt(jqThis[0].value) * 2;
                obj.calcData[dayNumberAsIndex]['meeting_pax'] = obj.calcData[dayNumberAsIndex]['total_pax'] - obj.calcData[dayNumberAsIndex]['staying_pax'];
                obj.inputControls[dayNumberAsIndex]['pax'].min = obj.calcData[dayNumberAsIndex]['staying_pax'] + 1;

                _set_unitOnCalculator(jqNodeType, 'accommodation', dayNumberAsIndex);
              }
              else if( parseInt(jqThis[0].value) * 2 >= obj.calcData[dayNumberAsIndex]['total_pax'] ) {
                var residentialConfirm = confirm(jqThis[0].value + ' akomodasi kamar dapat menampung seluruh peserta. Kami menyarankan Anda menggunakan paket Residential untuk seluruh peserta.\nApakah Anda ingin mengubahnya?');

                if(residentialConfirm) {
                  obj.inputControls[dayNumberAsIndex]['type'].value = jq( obj.inputControls[dayNumberAsIndex]['type'] ).find('option[data-price-tag=Residential]')[0].value;
                  obj.inputControls[dayNumberAsIndex]['pax'].min = 2;

                  jq( obj.inputControls[dayNumberAsIndex]['type'] ).trigger('change');
                }
                else {
                  jqThis[0].value = parseInt(jqThis[0].value) - 1;
                }
              }
            }
          }
          else if( jqNodeType === 'checkbox' ) {
            if( jqThis[0].checked ) {
              if( parseInt( obj.inputControls[dayNumberAsIndex]['pax'].value ) > 2 ) {
                obj.calcData[dayNumberAsIndex]['staying_pax'] = 2;
                obj.calcData[dayNumberAsIndex]['meeting_pax'] -= 2;
                obj.inputControls[dayNumberAsIndex]['pax'].min = obj.calcData[dayNumberAsIndex]['staying_pax'] + 1;

                _toggleAccommodationRoomField('show', dayNumberAsIndex, true);
              }
              else {
                obj.inputControls[dayNumberAsIndex]['type'].value = jq( obj.inputControls[dayNumberAsIndex]['type'] ).find('option[data-price-tag=Residential]')[0].value;

                jq( obj.inputControls[dayNumberAsIndex]['type'] ).trigger('change');
              }
            }
            else {
              obj.calcData[dayNumberAsIndex]['staying_pax'] = 0;
              obj.calcData[dayNumberAsIndex]['meeting_pax'] = obj.calcData[dayNumberAsIndex]['total_pax'];
              obj.inputControls[dayNumberAsIndex]['pax'].min = 2;

              _toggleAccommodationRoomField('hide', dayNumberAsIndex, true);
            }
          }
        break;

        case 'select':
          var selectedMeetingType = jqThis[0].options[ jqThis[0].selectedIndex ];
          jqNodeType = 'select';
          obj.calcData[dayNumberAsIndex]['meeting_type'] = selectedMeetingType.dataset.priceTag;
          obj.calcData[dayNumberAsIndex]['price_idr'] = parseInt( selectedMeetingType.dataset.priceIdr );

          if( obj.calcData[dayNumberAsIndex]['meeting_type'] === 'Residential' ) {
            obj.calcData[dayNumberAsIndex]['staying_pax'] = obj.calcData[dayNumberAsIndex]['total_pax'];
            obj.calcData[dayNumberAsIndex]['meeting_pax'] = 0;

            _toggleAccommodationRoomField('hide', dayNumberAsIndex);
          }
          else {
            if( obj.inputControls[dayNumberAsIndex].hasOwnProperty('accommodation') && obj.inputControls[dayNumberAsIndex]['accommodation'].checked == false ) {
              obj.calcData[dayNumberAsIndex]['staying_pax'] = 0;
              obj.calcData[dayNumberAsIndex]['meeting_pax'] = obj.calcData[dayNumberAsIndex]['total_pax'];
            }

            _toggleAccommodationRoomField('show', dayNumberAsIndex);
          }

          _set_unitOnCalculator(jqNodeType, 'meeting', dayNumberAsIndex);
        break;
      }

      if( obj.calcData[dayNumberAsIndex]['disc_idr'] > 0 ) {
        if( settings.priceCategory.toLowerCase() === 'perpax' ) {
          obj.calcData[dayNumberAsIndex]['disc_idr'] = (settings.weekendDiscount / 100) * ( (obj.calcData[dayNumberAsIndex]['price_idr'] * obj.calcData[dayNumberAsIndex]['meeting_pax']) + (settings.accommodationPrice * obj.calcData[dayNumberAsIndex]['staying_pax']) );
          
          _set_unitOnCalculator('discount', 'discount', dayNumberAsIndex);
        }
        else if( settings.priceCategory.toLowerCase() === 'perroom' && jqNodeType === 'select' ) {
          obj.calcData[dayNumberAsIndex]['disc_idr'] = (settings.weekendDiscount / 100) * obj.calcData[dayNumberAsIndex]['price_idr'];
          
          _set_unitOnCalculator('discount', 'discount', dayNumberAsIndex);
        }
      }

      _set_totalPrice();

      console.table(obj.calcData);

      _set_finalPriceUnitOnCalculator();

      jqThis.off( obj.event.change );
    });

    // Call the constructor to begin the initialization of plugin
    _construct();
  };

  /**
   * @param options
   * @returns {*}
   * @constructor
   */
  jq.fn['VKCalculator'] = function(options) {
    // Add unique class to calculator and fields container first, then
    // prepend the loading animation
    var loadingMarkup = '<div class="calculator-init-loading"><img src="/Content2016/Images/layout/ajaxloader-blue.gif" width="32" height="32"></div>';

    options.fieldsContainer.addClass('vkcalculator__dataSource').prepend(loadingMarkup);
    options.calculatorContainer.addClass('vkcalculator calculator-container').prepend(loadingMarkup);

    return this.each(function() {
      if ( ! jq.data(this, 'plugin_VKCalculator')) {
        jq.data( this, 'plugin_VKCalculator', new VKCalculator(this, options) );
      }
    });
  };
})( jQuery, window, document );